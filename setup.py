from setuptools import setup, find_packages

setup(
    name = 'pypyrsftp',
    version = '0.1.5',
    description = 'steps for the pypyr task runner to enable sftp upload',
    author = 'Florian Korbacher',
    packages = find_packages(exclude=['tests']),
    keywords = 'task-runner,automation,devops,ci/cd,pipeline runner,sftp',
    python_requires = '>=3.8',
    install_requires = ['pysftp', 'pypyr']
)
