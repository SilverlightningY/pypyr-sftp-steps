import logging

from pypyr.context import Context

from pypyrsftp.lib.connection import PypyrSFTPConnection
from pypyrsftp.lib.context_properties import Local, Remote
from pypyrsftp.lib.path import (
    create_destination_path,
    filter_not_match_glob,
    resolve_glob_to_paths,
)

logger = logging.getLogger(__name__)
step_name = 'upload'


def run_step(context: Context) -> None:
    logger.debug('started')

    local = Local(context, step_name)
    local_root_directory = local.get_root_dir(required = True)
    logger.debug(f'local root directory: {local_root_directory}')
    local_include_glob = local.get_include_glob()
    logger.debug(f'local include glob: {str(local_include_glob)}')
    local_exclude_glob = local.get_exclude_glob()
    logger.debug(f'local exclude glob: {str(local_exclude_glob)}')
    remote = Remote(context, step_name)
    remote_target_directory = remote.get_target_directory(required = True)
    logger.debug(f'remote target directory: {remote_target_directory}')

    include_paths = resolve_glob_to_paths(local_root_directory, local_include_glob)
    if (logger.getEffectiveLevel() == logging.DEBUG):
        logger.debug('include paths before filtering:')
        for p in include_paths:
            logger.debug(str(p))

    if (local_exclude_glob):
        include_paths = filter_not_match_glob(include_paths, local_exclude_glob)
        if (logger.getEffectiveLevel() == logging.DEBUG):
            logger.debug('include paths after filtering:')
            for p in include_paths:
                logger.debug(str(p))

    with PypyrSFTPConnection(context, step_name) as connection:
        for path in include_paths:
            destination_path = create_destination_path(
                path, local_root_directory, remote_target_directory)
            if not connection.exists(str(remote_target_directory)):
                connection.mkdir(str(remote_target_directory))
            if path.is_dir():
                logger.debug(f'create directory at {destination_path}')
                connection.mkdir(str(destination_path))
            elif path.is_file():
                logger.debug(f'upload {path} to {destination_path}')
                connection.put(str(path), str(destination_path))

        if len(include_paths) > 0:
            logger.info(f'{len(include_paths)} paths successfully uploaded')
        else:
            logger.info('no files to upload')

    logger.debug('done')
