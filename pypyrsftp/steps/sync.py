import logging
from pathlib import Path
from typing import Union

from pypyr.context import Context

from pypyrsftp.lib.connection import PypyrSFTPConnection
from pypyrsftp.lib.context_properties import Local, Remote
from pypyrsftp.lib.path import (
    filter_not_match_glob,
    path_match_any_glob,
    path_match_glob,
    resolve_glob_to_paths,
    create_destination_path,
)
from pypyrsftp.lib.sync import SyncStatus

logger = logging.getLogger(__name__)
step_name = 'sync'

def run_step(context: Context) -> None:
    logger.debug('started')
    local = Local(context, step_name)
    local_root_directory = local.get_root_dir(required = True)
    logger.debug(f'local root directory: {local_root_directory}')
    include_glob: Union[str, list[str]] = context.get('includeGlob', '**/*')
    logger.debug(f'include glob: {include_glob}')
    exclude_glob: Union[str, list[str], None] = context.get('excludeGlob')
    logger.debug(f'exclude glob: {exclude_glob}')
    remote = Remote(context, step_name);
    remote_target_directory = remote.get_target_directory(required = True)
    logger.debug(f'remote target directory: {remote_target_directory}')
    
    local_include_paths = resolve_glob_to_paths(local_root_directory, include_glob)
    if logger.getEffectiveLevel() == logging.DEBUG:
        logger.debug('local include paths:')
        for path in local_include_paths:
            logger.debug(path)

    if (exclude_glob):
        local_include_paths = filter_not_match_glob(local_include_paths, exclude_glob)
        if logger.getEffectiveLevel() == logging.DEBUG:
            logger.debug('local include paths after filtering:')
            for path in local_include_paths:
                logger.debug(path)

    sync_states: dict[str, SyncStatus] = dict()
    for local_path in local_include_paths:
        relative_path = local_path.relative_to(local_root_directory)
        sync_states[str(relative_path)] = SyncStatus(local_path = local_path)

    def add_item_to_sync_states(remote_path: Path) -> None:
        logger.debug('add ' + str(remote_path))
        relative_path = remote_path.relative_to(remote_target_directory)
        if sync_states.get(str(relative_path)):
            sync_states[str(relative_path)].set_remote_path(remote_path)
        else:
            sync_states[str(relative_path)] = SyncStatus(remote_path = remote_path)

    def remote_path_match_glob(path: Path, glob: Union[str, list[str]]) -> bool:
        if isinstance(glob, list):
            return path_match_any_glob(path, glob)
        elif isinstance(glob, str):
            return path_match_glob(path, glob)
        raise TypeError(f'glob must be of type str or list[str], but was {type(glob)}')

    with PypyrSFTPConnection(context, step_name) as connection:
        if not connection.exists(str(remote_target_directory)):
            connection.mkdir(str(remote_target_directory))

        def on_item(path: str) -> None:
            remote_path = Path(path)
            if exclude_glob and not remote_path_match_glob(remote_path, exclude_glob):
                add_item_to_sync_states(remote_path)
        logger.info('query remote filetree info')
        logger.debug('remote paths with filter:')
        connection.walktree(str(remote_target_directory), on_item, on_item, on_item, True)

        sync_states_to_upload = [s for s in sync_states.values() if s.is_local() and not s.is_remote()]
        if logger.getEffectiveLevel() == logging.DEBUG:
            logger.debug('sync states to upload:')
            for sync_status in sync_states_to_upload:
                logger.debug(f'local path: {sync_status.get_local_path()}')
        sync_states_to_delete = [s for s in sync_states.values() if not s.is_local()]
        sync_states_to_delete.reverse()
        if logger.getEffectiveLevel() == logging.DEBUG:
            logger.debug('sync states to delete:')
            for sync_status in sync_states_to_delete:
                logger.debug(f'remote path: {sync_status.get_remote_path()}')

        for sync_status in sync_states_to_upload:
            local_path = sync_status.get_local_path()
            if local_path:
                remote_path = create_destination_path(local_path, local_root_directory, remote_target_directory)
                if local_path.is_dir():
                    connection.mkdir(str(remote_path))
                elif local_path.is_file():
                    connection.put(str(local_path), str(remote_path))
        if len(sync_states_to_upload) > 0:
            logger.info(f'{len(sync_states_to_upload)} paths successfully uploaded')
        else:
            logger.info('no paths to upload')

        for sync_status in sync_states_to_delete:
            remote_path = sync_status.get_remote_path()
            if remote_path:
                if connection.isdir(str(remote_path)):
                    connection.rmdir(str(remote_path))
                if connection.isfile(str(remote_path)):
                    connection.remove(str(remote_path))
        if len(sync_states_to_delete) > 0:
            logger.info(f'{len(sync_states_to_delete)} paths successfully deleted')
        else:
            logger.info('no paths to delete')

    logger.debug('done')
