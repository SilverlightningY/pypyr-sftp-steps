import logging
from pathlib import Path
from typing import Callable

from pypyr.context import Context

from pypyrsftp.lib.connection import PypyrSFTPConnection
from pypyrsftp.lib.path import (
    create_destination_path,
    filter_not_match_glob,
    resolve_glob_to_paths,
)
from pypyrsftp.lib.context_properties import (
    Local,
    Remote,
)

logger = logging.getLogger(__name__)
step_name = 'uploadwithbackup'

def create_remote_path_function(local_root_directory: Path, remote_root_directory: Path) -> Callable[[Path], Path]:
    return lambda path: create_destination_path(path, local_root_directory, remote_root_directory)

def run_step(context: Context) -> None:
    logger.debug('started')
    # assert required properties
    local = Local(context, step_name)
    local_root_directory = local.get_root_dir(required = True)
    local_include_glob = local.get_include_glob()
    local_exclude_glob = local.get_exclude_glob()
    remote = Remote(context, step_name)
    remote_backups_directory = remote.get_backups_directory(required = True)
    remote_backup_name = remote.get_backup_name(required = True)
    remote_target_directory = remote.get_target_directory(required = True)
    remote_static_directory = remote.get_static_directory()
    remote_temp_directory = remote.get_temp_directory()

    # the local path which the glob paths are relative to
    logger.debug('local root directory: ' + str(local_root_directory))

    # paths destined for upload
    include_paths = resolve_glob_to_paths(
        local_root_directory, local_include_glob)
    if (logger.getEffectiveLevel() == logging.DEBUG):
        logger.debug('included paths:')
        for p in include_paths:
            logger.debug(str(p))

    # paths not destined for upload
    if (local_exclude_glob):
        if (logger.getEffectiveLevel() == logging.DEBUG 
                and isinstance(local_exclude_glob, list)):
            for glob in local_exclude_glob:
                logger.debug('exclude glob set to: ' + glob)
        else:
            logger.debug('exclude glob set to: ' + str(local_exclude_glob))
        include_paths = filter_not_match_glob(include_paths, local_exclude_glob)
        if (logger.getEffectiveLevel() == logging.DEBUG):
            logger.debug('include paths after filtering exclude paths')
            for p in include_paths:
                logger.debug(str(p))

    logger.debug(f'remote temp directory: {remote_temp_directory}')
    create_remote_path = create_remote_path_function(local_root_directory, remote_temp_directory)
    remote_paths = [create_remote_path(p) for p in include_paths]
    if (logger.getEffectiveLevel() == logging.DEBUG):
        logger.debug('remote paths')
        for p in remote_paths:
            logger.debug(str(p))

    current_backup_directory = remote_backups_directory / remote_backup_name
    logger.debug(f'current backup directory: {current_backup_directory}')

    with PypyrSFTPConnection(context, step_name) as connection:
        if (connection.exists(str(remote_temp_directory))):
            raise RuntimeError(f'Upload aborted because tempDir {remote_temp_directory} already exists on the server.')
        if (connection.exists(str(current_backup_directory))):
            raise RuntimeError(f'Upload aborted because backupDir {current_backup_directory} already exists on the server.')
        connection.mkdir(str(remote_temp_directory))
        if not connection.exists(str(remote_backups_directory)):
            connection.mkdir(str(remote_backups_directory))
        for local_path, remote_path in zip(include_paths, remote_paths):
            if (local_path.is_dir()):
                logger.debug(f'create remote directory {remote_path}')
                connection.mkdir(str(remote_path))
            else:
                logger.debug(f'upload file {local_path} to {remote_path}')
                connection.put(str(local_path), str(remote_path))
        
        if (remote_static_directory):
            logger.debug('static directory present')
            def move_static_directory(static_dir: Path) -> None:
                if connection.exists(str(static_dir)):
                    relative_dir = static_dir.relative_to(remote_target_directory)
                    destination_dir = remote_temp_directory / relative_dir
                    logger.debug(f'rename static dir {static_dir} to {destination_dir}')
                    connection.rename(str(static_dir), str(destination_dir))

            if isinstance(remote_static_directory, list):
                for dir in remote_static_directory:
                    move_static_directory(dir)
            else:
                move_static_directory(remote_static_directory)
        
        if connection.exists(str(remote_target_directory)):
            # move old target to backup directory
            connection.rename(str(remote_target_directory), str(current_backup_directory))
            logger.debug(f'{remote_target_directory} to {current_backup_directory} renamed')
            logger.info(f'backup saved at {current_backup_directory}')
        # make temp directory the new target
        connection.rename(str(remote_temp_directory), str(remote_target_directory))
        logger.debug(f'{remote_temp_directory} to {remote_target_directory} moved')
        logger.info(f'{len(include_paths)} paths successfully uploaded at {remote_target_directory}')

    logger.debug('done')
