from pypyr.context import Context
import pysftp
import logging

logger = logging.getLogger(__name__)

class PypyrSFTPConnection():
    def __init__(self, context: Context, step_name: str):
        context.assert_child_key_has_value(parent = 'sftpConnection', child = 'host', 
                                           caller = step_name)
        context.assert_child_key_has_value(parent = 'sftpConnection', child = 'username', 
                                       caller = step_name)
        sftp_connection_params = context.get_formatted('sftpConnection')
        self.connection = pysftp.Connection(**sftp_connection_params)

    def __enter__(self):
        return self.connection

    def __exit__(self, exception_type, exception_value, exception_traceback):
        self.connection.close()
        logger.debug(f'Connection closed because of {exception_type}: {exception_value}')
        logger.debug(exception_traceback)

    def __del__(self):
        self.connection.close()
        logger.debug('Connection closed because of deletion')

