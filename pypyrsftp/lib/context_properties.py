from abc import ABC
from pathlib import Path
from typing import Union

from pypyr.context import Context

class ContextParentProperty(ABC):
    def __init__(self, context: Context, step_name: str, key: str):
        self._context = context
        self._step_name = step_name
        self._key = key
        self._parent = context.get_formatted(key)

    def _get_property(
        self, 
        child: str, 
        required: bool, 
        fallback: Union[str, None] = None
    ) -> str:
        if required:
            self._context.assert_child_key_has_value(
                parent = self._key, child = child, caller = self._step_name)
        return self._parent.get(child, fallback)

class Local(ContextParentProperty):
    def __init__(self, context: Context, step_name: str):
        super().__init__(context, step_name, 'local')

    def get_root_dir(self, required = False) -> Path:
        root_dir = super()._get_property('rootDir', required)
        return Path(root_dir)

    def get_include_glob(self, required = False) -> Union[str, list[str]]:
        include_glob = super()._get_property(
            'includeGlob', required, fallback = '**/*')
        return include_glob

    def get_exclude_glob(self, required = False) -> Union[str, list[str]]:
        exclude_glob = super()._get_property('excludeGlob', required)
        return exclude_glob

class Remote(ContextParentProperty):
    def __init__(self, context: Context, step_name: str):
        super().__init__(context, step_name, 'remote')

    def get_backups_directory(self, required = False) -> Path:
        backups_directory = super()._get_property('backupsDir', required)
        return Path(backups_directory)

    def get_backup_name(self, required = False) -> str:
        backup_name = super()._get_property('backupName', required)
        return backup_name

    def get_target_directory(self, required = False) -> Path:
        target_directory = super()._get_property('targetDir', required)
        return Path(target_directory)

    def get_static_directory(self, required = False) -> Union[Path, list[Path], None]:
        static_directory = super()._get_property('staticDir', required)
        if static_directory:
            target_directory = self.get_target_directory(required = True)
            def make_path_relative_to_target_directory(path_string: str):
                path = Path(path_string)
                if not path.is_relative_to(target_directory):
                    return target_directory / path
                return path
            if isinstance(static_directory, list):
                return list(map(make_path_relative_to_target_directory, static_directory))
            return make_path_relative_to_target_directory(static_directory)

    def get_temp_directory(self, required = False) -> Path:
        temp_directory = super()._get_property('tempDir', required, fallback = 'next')
        return Path(temp_directory)

