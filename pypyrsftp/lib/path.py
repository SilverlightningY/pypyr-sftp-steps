import functools
import operator
import os
from pathlib import Path
from typing import Union

def filter_not_match_glob(
    paths: list[Path],
    glob: Union[str, list[str]]
) -> list[Path]:
    """
    Removes all paths which match the glob pattern.
    """
    if (isinstance(glob, list)):
        return [p for p in paths if not path_match_any_glob(p, glob)]
    elif (isinstance(glob, str)):
        return [p for p in paths if not path_match_glob(p, glob)]
    else:
        raise TypeError('glob must be of type str or list[str], but was ' + type(glob))

def resolve_glob_to_paths(
    root_path: Path, 
    glob: Union[str,list[str]]
) -> list[Path]:
    """
    Resolves glob pattern to Path relative to root_path. If glob is a list,
    the glob results are combined into a single list.
    """
    if (isinstance(glob, list)):
        path_list = [list(root_path.glob(p)) for p in glob]
        return functools.reduce(operator.add, path_list, [])
    else:
        return [p for p in root_path.glob(glob)]

def segment_matches(segment: str, pattern: str) -> bool:
    """
    Tests if the giben segment matches the pattern with a glob syntax. A
    pattern is a string between path seperators in a glob path pattern
    """
    if (segment == pattern):
        return True
    asterisk_mode = False
    pattern_index = 0
    segment_index = 0
    asterisk_mode_start_pattern_index = 0
    match_sequence_count = 0
    while segment_index < len(segment):
        if pattern_index >= len(pattern):
            # pattern is at the end but segment is not
            return False
        pattern_char = pattern[pattern_index]
        if pattern_char == '*':
            if (pattern_index == len(pattern) - 1):
                # last character in pattern is asterisk, therefore does
                # everything that comes next match the pattern
                return True
            asterisk_mode = True
            pattern_index += 1
            asterisk_mode_start_pattern_index = pattern_index
            continue
        segment_char = segment[segment_index]
        if asterisk_mode:
            if segment_char == pattern_char:
                pattern_index += 1
                match_sequence_count += 1
            elif match_sequence_count > 0:
                # set pattern_index back to where the partial match began
                pattern_index = asterisk_mode_start_pattern_index
                match_sequence_count = 0
                # do not increment the segment_index to try this character with
                # the beginning of the partial pattern again
                continue
        else:
            if segment_char == pattern_char:
                pattern_index += 1
            else:
                # exact match is impossible at the first mismatch
                return False
        segment_index += 1
    return pattern_index == len(pattern)

def path_match_glob(path: Path, glob: str) -> bool:
    """
    Tests if a path matches the glob path pattern. Relative glob patterns can
    match the path at any segment and has not to be the beginning of the path,
    even the path is relative too. E.g. pattern '*/abc/file.x' does match path
    'xyz/opo/abc/file.x'. Absolute paths only match absolute glob patterns.
    """
    path_segments = str(path).split(os.path.sep)
    glob_segments = glob.split(os.path.sep)
    glob_segments_len = len(glob_segments)
    glob_segment_index = 0
    for path_segment in path_segments:
        if (glob_segment_index >= glob_segments_len):
            return False
        glob_segment = glob_segments[glob_segment_index]
        if (glob_segment_index == 0):
            double_asterisk_mode = (glob_segment == '*' 
                                    or glob_segment == '**' 
                                    or glob_segment == '.')
        else: 
            double_asterisk_mode = glob_segment == '**'
        if (double_asterisk_mode):
            if (glob_segment_index == glob_segments_len - 1):
                # last segment in glob is double_asterisk, therefore does
                # anything that comes next match the pattern
                return True
            next_glob_segment = glob_segments[glob_segment_index + 1]
            if (segment_matches(path_segment, next_glob_segment)):
                glob_segment_index += 2
        else:
            if (segment_matches(path_segment, glob_segment)):
                glob_segment_index += 1
            else:
                return False
    return glob_segment_index == glob_segments_len

def path_match_any_glob(path: Path, globs: list[str]) -> bool:
    """
    Tests if the path matches any of the glob path patterns.
    """
    for glob in globs:
        if (path_match_glob(path, glob)):
            return True
    return False

def create_destination_path(
    path: Path, 
    source_root_directory: Path, 
    destination_root_directory: Path
) -> Path:
    relative_path = path.relative_to(source_root_directory)
    return destination_root_directory / relative_path
