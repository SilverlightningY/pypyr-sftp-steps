from pathlib import Path
from typing import Union

class SyncStatus():
    def __init__(self, local_path = None, remote_path = None):
        self.__local_path = local_path
        self.__remote_path = remote_path

    def is_local(self) -> bool:
        return self.__local_path != None

    def is_remote(self) -> bool:
        return self.__remote_path != None

    def set_local_path(self, path: Path):
        self.__local_path = path

    def set_remote_path(self, path: Path):
        self.__remote_path = path

    def get_local_path(self) -> Union[Path, None]:
        return self.__local_path

    def get_remote_path(self) -> Union[Path, None]:
        return self.__remote_path

