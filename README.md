# pypyr sftp steps
This project adds the capabilities of uploading files to a sftp server to the
pypyr pipeline framework. This helps especially by the deployment of websites
where things should be automated as far as possible to avoid human mistakes. 

## Usage
### common
This properties belong to each task.

#### sftpConnection
|property|datatype|required|description|
|-|-|-|-|
|`host`|`string`|`true`|The hostname of the ssh server.|
|`port`|`int`|`false`|The port of the ssh server. Default: `22`.|
|`username`|`string`|`true`|The username of the ssh server user account.|
|`password`|`string`|`false`|The password of the ssh server user account. Key exchange is recommended.|

### uploadwithbackup
Example configuration:
```yaml
- step: pypyrsftp.steps.uploadwithbackup
  in:
    sftpConnection:
      host: <hostname>
      username: <username>
    local:
      rootDir: './path/to/root/'
      includeGlob: '**/*'
      excludeGlob: 
        - '*/static/**'
        - '*/static'
    remote:
      backupsDir: backups
      tempDir: temp
      backupName: 'b_{now}'
      targetDir: public
      staticDir: static
```
#### local
|property|datatype|required|description|
|-|-|-|-|
|`includeGlob`|<code>string &#124; list&lt;string&gt;</code>|`false`|The path(s) that should be uploaded. Default to all files recursive in the `rootDir`.|
|`excludeGlob`|<code>string &#124; list&lt;string&gt;</code>|`false`|The path(s) that should be ignored for uploading. Relative paths don't have to start at the same directory as the `includeGlob` property.|
|`rootDir`|`string`|`true`|The directory where the upload files are reside in. The `includeGlob` is relative to this directory.|

#### remote
|property|datatype|required|description|
|-|-|-|-|
|`backupsDir`|`string`|`true`|The directory where the backups should be stored in.|
|`tempDir`|`string`|`false`|The directory where the files are temporary uploaded to minimize the downtime. It is renamed to `targetDir` after successful upload. Default: `/next`|
|`targetDir`|`string`|`true`|The directory where the uploaded files should be in afterwards.|
|`staticDir`|<code>string &#124; list&lt;string&gt;</code>|`false`|The path(s) that is/are unchainged through uploads. It/They do not appear in the backups. It is relative to `targetDir`.|

### upload
Example configuration:
```yaml
- step: pypyrsftp.steps.upload
  in:
    sftpConnection:
      host: <hostname>
      username: <username>
    local:
      rootDir: './path/to/root/public'
      includeGlob: '**/*'
      excludeGlob: '*/*.png'
    remote:
      targetDir: public/static/images
```
#### local
|property|datatype|required|description|
|-|-|-|-|
|`includeGlob`|<code>string &#124; list&lt;string&gt;</code>|`true`|The path(s) that should be uploaded.|
|`excludeGlob`|<code>string &#124; list&lt;string&gt;</code>|`false`|The path(s) that should be ignored for uploading. Relative paths don't have to start at the same directory as the `includeGlob` property.|
|`rootDir`|`string`|`true`|The directory where the upload files are reside in. The `includeGlob` is relative to this directory.|

#### remote
|property|datatype|required|description|
|`staticDir`|<code>string &#124; list&lt;string&gt;</code>|`false`|The path(s) that is/are unchainged through uploads. It/They do not appear in the backups. It is relative to `targetDir`.|

### sync
Synchronizes a local and a remote directory.

Example configuration:
```yaml
- step: pypyrsftp.steps.sync
  in:
    sftpConnection:
      host: <hostname>
      username: <username>
    excludeGlob: 
      - '*/static/images/**'
      - '*/static/images'
    local:
      rootDir: './path/to/root/static'
    remote:
      targetDir: public/static
```
#### root
|property|datatype|required|description|
|`excludeGlob`|<code>string &#124; list&lt;string&gt;</code>|`false`|These files are not part of the sync and are ignored on local and remote side.|

#### local
|property|datatype|required|description|
|`rootDir`|`string`|`true`|The root directory where the files to sync are. This is the sync source. Every file that gets deleted here will be deleted on the remote counterpart too.|

#### remote
|property|datatype|required|description|
|`targetDir`|`string`|`true`|The destination for the sync.|
## State of the project
This project is in early development phase and lacks a lot of functionality.
