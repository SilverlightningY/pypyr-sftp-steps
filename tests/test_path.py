from pathlib import Path
from unittest import TestCase

from pypyrsftp.lib.path import (
    create_destination_path,
    filter_not_match_glob,
    path_match_glob,
    segment_matches,
)

class TestPath(TestCase):

    def test_filter_not_matches_glob(self):
        paths = [
            Path('./path/xyz/file.b'),
            Path('./path/xyz/bfile.c'),
            Path('./path/xyz/cfile'),
            Path('./path/xyz/dir/file.c'),
            Path('./path/xyz/dir/dir/file.c'),
            Path('./path/abc/file.b'),
            Path('./path/xyz/dir/dir/dir.c'),
            Path('./path/xyz/dfile.c'),
            Path('./path/def/file.c')]
        result_paths = filter_not_match_glob(paths, 'path/xyz/**')
        self.assertEqual(len(result_paths), 2, f'filter function has not filtered all targets')
        self.assertEqual(result_paths[0], paths[5], f'wrong element at index 0')
        self.assertEqual(result_paths[1], paths[8], f'wrong element at index 1')

    def test_path_match_glob_absolute_path(self):
        path = Path('/abc/def/xyz/file.py')
        glob = '*/xyz/*.py'
        self.assertTrue(path_match_glob(path, glob), f'absolute path {path} does not match pattern {glob}')
        glob = '/abc/def/xyz/file.py'
        self.assertTrue(path_match_glob(path, glob), f'absolute path {path} does not match pattern {glob}')

    def test_path_match_glob_relative_path(self):
        path = Path('abc/def/xyz/file.ext')
        glob = 'abc/*/xyz/*.ext'
        self.assertTrue(path_match_glob(path, glob), f'relative path {path} does not match pattern {glob}')
        glob = 'abc/**/*.ext'
        self.assertTrue(path_match_glob(path, glob), f'relative path {path} does not match pattern {glob}')
        glob = './xyz/file.ext'
        self.assertTrue(path_match_glob(path, glob), f'relative path {path} does not match pattern {glob}')

    def test_segment_matches(self):
        segment = ''
        pattern = ''
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        segment = 'abcd'
        pattern = '*cd'
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        pattern = 'ab*'
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        pattern = '*'
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        pattern = '**'
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        pattern = 'a*d'
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        pattern = ''
        self.assertFalse(segment_matches(segment, pattern), f'segment \'{segment}\' does match pattern \'{pattern}\'')
        pattern = 'xde'
        self.assertFalse(segment_matches(segment, pattern), f'segment \'{segment}\' does match pattern \'{pattern}\'')
        segment = ''
        self.assertFalse(segment_matches(segment, pattern), f'segment \'{segment}\' does match pattern \'{pattern}\'')
        segment = 'hhhahallo'
        pattern = '*hallo'
        self.assertTrue(segment_matches(segment, pattern), f'segment \'{segment}\' does not match pattern \'{pattern}\'')
        pattern = '*hall'
        self.assertFalse(segment_matches(segment, pattern), f'segment \'{segment}\' does match pattern \'{pattern}\'')
        segment = 'fsafehall'
        pattern = '*hallo'
        self.assertFalse(segment_matches(segment, pattern), f'segment \'{segment}\' does match pattern \'{pattern}\'')

    def test_create_destination_path(self):
        path = Path('source/my/file.xyz')
        source_root_directory = Path('source')
        destination_root_directory = Path('destination')
        destination_path = create_destination_path(path, source_root_directory, destination_root_directory)
        self.assertEqual(Path('destination/my/file.xyz'), destination_path)

        source_root_directory = Path('source/my')
        destination_path = create_destination_path(path, source_root_directory, destination_root_directory)
        self.assertEqual(Path('destination/file.xyz'), destination_path)

        destination_root_directory = Path('des/ti/na/tion')
        destination_path = create_destination_path(path, source_root_directory, destination_root_directory)
        self.assertEqual(Path('des/ti/na/tion/file.xyz'), destination_path)

